class UserController < ApplicationController
	before_filter :admin_filter

	def admin_filter
		if !user_signed_in?
			redirect_to public_index_path
		end
	end
  	def index
  		apikey = current_user.access_token
  		@graph = Koala::Facebook::API.new(apikey)
  		@feed = @graph.get_connections("me", "feed")
  		@feed.each do |f|  			
  			json_data = f.to_json
  			f["likes"] ? likes = f["likes"]["data"].count : likes = 0
  			if post = current_user.posts.find_by_fb_id(f["id"])
  				if post.likes == likes && post.json_data == json_data
  					next
  				else
  					post.likes = likes
  					post.json_data = json_data
  					post.save!
  				end
  			else
  				post = current_user.posts.new
  				post.likes = likes
  				post.fb_id = f["id"]
  				post.json_data = f.to_json
  				post.save!
  			end
  		end
  		@posts = current_user.posts.order(likes: :desc)  		

  	end
  	def after_sign_in
  		redirect_to user_index_path
  	end
end
