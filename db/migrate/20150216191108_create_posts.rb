class CreatePosts < ActiveRecord::Migration
	def change
		create_table :posts do |t|
			t.belongs_to :user
			t.string :fb_id
			t.text :json_data
			t.integer :likes

			t.timestamps
		end
	end
end
